# frozen_string_literal: true

module UnlabelledIssuesHelper
  POTENTIAL_TRIAGERS =
    %w(
      @a_mcdonald
      @at.ramya
      @caalberts
      @dchevalier2
      @ddavison
      @ebanks
      @godfat
      @grantyoung
      @jennielouie
      @jo_shih
      @kwiebers
      @markglenfletcher
      @mlapierre
      @niskhakova
      @rymai
      @sliaquat
      @svistas
      @tmslvnkc
      @tpazitny
      @treagitlab
      @vincywilson
      @wlsf82
      @zeffmorgan
    ).freeze
end
