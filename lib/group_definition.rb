# frozen_string_literal: true

module GroupDefinition
  module_function

  DATA = YAML.load_file(File.expand_path("#{__dir__}/../group-definition.yml")).freeze

  DATA.each do |name, definition|
    define_method("group_#{name}") do
      result = {
        assignees:
          definition.values_at(
            'pm',
            'engineering_manager',
            'backend_engineering_manager',
            'frontend_engineering_manager',
            'extra_assignees'
          ).flatten.compact.uniq,
        labels:
          definition['labels'] || ["group::#{name.tr('_', ' ')}"]
      }

      extra_mentions = definition['extra_mentions']

      if extra_mentions
        result[:mentions] = result[:assignees].concat(extra_mentions).uniq
      end

      result
    end
  end
end
