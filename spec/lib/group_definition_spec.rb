require 'spec_helper'
require 'group_definition'

RSpec.describe GroupDefinition do
  subject(:definition) { described_class.public_send(definition_name) }

  GroupDefinition.methods(false).each do |method_name|
    describe "##{method_name}" do
      let(:definition_name) { method_name }

      it 'returns assignees and labels as arrays, optionally mentions too' do
        expect(definition).to include(
          assignees: a_kind_of(Array),
          labels: a_kind_of(Array)
        )

        expect(definition[:mentions]).to be_a_kind_of(Array).or(be_nil)
      end
    end
  end
end
